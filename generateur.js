let Math = require('math');


function occurence(min, max, taille) {
    const liste=[];
    for (i=0; i<taille; i++){
        let x = Math.floor(Math.random()*(max-min+1)+min);
        liste.push(x)       
    }
    //console.log(liste)
    let occMax = 0;
    let occMin = liste.length;
    let m=0;
    let itemMin;
    let itemMax;
    for (let j=0; j<liste.length; j++) {
        for (let k=0; k<liste.length; k++){
            if (liste[j] == liste[k]){
                m++               
            }

        }
        if (m>occMax){
            occMax=m
            itemMax=liste[j]
        } 
        if(m<occMin){
            occMin=m
            itemMin=liste[j]
        }
        m=0;
    }
    let array = {itemMin:itemMin, occMin:occMin, itemMax:itemMax,occMax:occMax}
    return array
}

function ecart(nbEchantillon, min, max, taille){
    let plusPetitEcart = taille;
    let plusGrandEcart = 0;
    let sommeEcart = 0;
    for (let i=1; i<= nbEchantillon; i++) {
        let array = occurence (min, max, taille)
        //console.log(array)
        let ecart = array["occMax"] - array["occMin"];
        sommeEcart=  sommeEcart + ecart
        if (ecart>plusGrandEcart){
            plusGrandEcart = ecart
        }
        if (ecart<plusPetitEcart){
            plusPetitEcart=ecart
        }
    }
    let moyenneEcart = sommeEcart/nbEchantillon;
    console.log(" le plus grand écart est:",plusGrandEcart,'\n',"le plus petit écart est:",plusPetitEcart,'\n',"la moyenne des écarts est:",moyenneEcart)
}

function echantillon (min,max) {
    taille= [5,10,20,40,80,160]
    for(const element of taille) {
        let array = occurence (min, max, element)
        let frequenceMax = (array["occMax"]/element)*100;
        let frequenceMin = (array["occMin"]/element)*100;
        console.log("lorsque la taille de l'echatillon est", element, "frequence maximale:", frequenceMax,"%", "et frequence minimale:",frequenceMin,"%")
    }
}
